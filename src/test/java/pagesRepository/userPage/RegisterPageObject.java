package pagesRepository.userPage;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;

import com.itay.Utils.ExcelElement;
import com.itay.Utils.Elements.TextBoxForm;

import pagesRepository.BasePageObject;


public class RegisterPageObject extends BasePageObject {
	
	@FindBy(css = ".panel-heading")
	private WebElement lSignUp;
	
	@FindBy(name = "firstname")
	private WebElement fFirstName;
	
	@FindBy(name = "lastname")
	private WebElement fLastName;
	
	@FindBy(name = "phone")
	private WebElement fPhoneNumber;
	
	@FindBy(name = "email")
	private WebElement fEmail;
	
	@FindBy(name = "password")
	private WebElement fPassword;
	
	@FindBy(name = "confirmpassword")
	private WebElement fConfirmPassword;
	
	@FindBy(css = "#headersignupform > div:nth-child(9) > button")
	private WebElement bSubmitSignUp;
	
	
	public RegisterPageObject(){
		wait.until(ExpectedConditions.urlToBe("http://www.phptravels.net/register"));
	}
	
	public RegisterPageObject AssertlSignUpExists()
	{
		Assert.assertEquals(lSignUp.getText(), "Sign Up");
		return this;
	}
	
	protected void submitRegisterForms(WebElement e, String text)
	{
		e.clear();
		e.sendKeys(text);
	}
	
	protected void clickSubmitButton()
	{
		scrollToElement(bSubmitSignUp);
		bSubmitSignUp.click();
		reportLog("Clicked on Sign up Button",true);
	}
	/**
	 * Entering parameters to the form, in case of putting a field empty, send to a parameter Null
	 * @param firstname
	 * @param lastname
	 * @param phonenumber
	 * @param email
	 * @param password
	 */
	public RegisterPageObject submitForForms(String firstname,String lastname,String phonenumber,String email,
			String password,String confirmpassword){
		
		if(firstname != null)
		{
			submitRegisterForms(ExcelElement.get("First name"),firstname);
			reportLog("Submitted Firstname",true);
		}
		if(lastname != null)
		{
			submitRegisterForms(fLastName,lastname);
			reportLog("Submitted LastName",true);
		}
		if(phonenumber != null)
		{
			submitRegisterForms(fPhoneNumber,phonenumber);
			reportLog("Submitted Phone Number",true);
		}
		if(email != null)
		{
			submitRegisterForms(fEmail,email);
			reportLog("Submitted Email",true);
		}
		if(password != null)
		{
			submitRegisterForms(fPassword,password);
			reportLog("Submitted Password",true);
		}
		if(confirmpassword != null)
		{
			submitRegisterForms(fConfirmPassword,confirmpassword);
			reportLog("Submitted Confirm password",true);
		}
		return this;
	}
	public AccountPageObject clickOnSubmitButton(){
		
		clickSubmitButton();
		return new AccountPageObject();
	}
	

}
