package pagesRepository.userPage;

import java.util.HashMap;
import java.util.Map;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.testng.Assert;

import pagesRepository.BasePageObject;


public class VerifyAccountPageObject extends BasePageObject {
	
	@FindBy(css = "a[href='#bookings']")
	WebElement bBookingButton;
	
	@FindBy(css = "a[href='#profile']")
	WebElement bMyProfileButton;
	
	@FindBy(css = "a[href='#wishlist']")
	WebElement bWishlistButton;
	
	@FindBy(css = "a[href='#newsletter']")
	WebElement bNewsletterButton;
	
	@FindBy(how = How.CSS , using = "a[href='#newsletter']")
	WebElement bNewslettearButton;

	
	public AccountPageObject VerifyAccountPage(String firstname,String lastname,String phonenumber,String email,
			String password){
		
		Map<WebElement,String> elements = new HashMap<WebElement,String>();
		elements.put(bBookingButton, "Booking Button");
		elements.put(bMyProfileButton, "Profile Button");
		elements.put(bWishlistButton, "Wishlist Button");
		elements.put(bNewsletterButton, "Newsletter Button");
		
		
		for (Map.Entry<WebElement, String> entry : elements.entrySet())
		{
			try{
			    Assert.assertTrue(entry.getKey().isDisplayed());
			    reportLog(entry.getValue() + "exists",true);
			}
			catch(AssertionError e){
				reportLog(entry.getValue() + " does not exist",false);
			}
		

		}


		return new AccountPageObject();
	}
	


}
