package pagesRepository.userPage;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.Assert;


import pagesRepository.BasePageObject;

public class AccountPageObject extends BasePageObject {
	
	@FindBy(css = ".col-md-6.go-right.RTL > h3")
	WebElement lUserHeadline;
	
	public AccountPageObject(){
		wait.until(ExpectedConditions.urlToBe("http://www.phptravels.net/account/"));
	}
	
	public AccountPageObject verifyTextInAccountWindow(String firstname,String lastname){
		try{
			Assert.assertTrue(containText(lUserHeadline.getText(),firstname));
			reportLog("First name exists in the text in the page",true);
		}
		catch(AssertionError e){
			reportLog("First name does not exist in the text in the page",false);
		}
		try{
			Assert.assertTrue(containText(lUserHeadline.getText(),lastname));
			reportLog("Last name exists in the text in the page",true);
		}
		catch(AssertionError e){
			reportLog("Last name does not exist in the text in the page",false);
		}

		
		
		return this;
	}
	public AccountPageObject verifyTextAccountButton(String firstname){
		
		try{
			Assert.assertEquals(buttonMyAccount.getText(),makeUpperChar(firstname));
			reportLog("First name exists in the Account button",true);
		}
		catch(AssertionError e){
			reportLog("First name does not exist in the Account button",false);
		}

		
		return this;
	}
	public AccountPageObject verifyLogoExists(){
		
		try{
			Assert.assertTrue(iLogoSite.isDisplayed());;
			reportLog("Logo Exists",true);
		}
		catch(AssertionError e){
			reportLog("Logo does not Exists",false);
		}

		
		return this;
	}
	public boolean containText(String sElementText,String sExpected){
		if(sElementText.contains(sExpected)){
			return true;
		}
		return false;
	}
	public VerifyAccountPageObject VerifyAccountPage(){
		return new VerifyAccountPageObject();
	}
	
}
