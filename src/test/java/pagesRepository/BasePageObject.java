package pagesRepository;

import java.io.IOException;
import java.util.Random;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.itay.Utils.CaptureScreenshot;
import com.itay.Utils.ConstVaribles;
import com.itay.Utils.ExcelElement;
import com.itay.Utils.ReportClassConfig;
import com.itay.Utils.TestBaseClass;

import pagesRepository.homePage.HomePage;
import pagesRepository.userPage.RegisterPageObject;

public class BasePageObject {
	
	protected WebDriver driver;
	protected WebDriverWait wait;
	protected ExtentTest reporter;
	
	public BasePageObject(){
		
		this.driver = TestBaseClass.getDriver();
		
		PageFactory.initElements(driver, this);
		
		wait = new WebDriverWait(driver,ConstVaribles.SECONDS_EXPLICIT_WAIT);
		
		reporter = ReportClassConfig.reporter();
	}
	
	@FindBy(css = "img[alt='PHPTRAVELS']")
	protected WebElement iLogoSite;
	
	@FindBy(css = ".navbar-collapse.collapse > ul > ul > li:nth-child(1)")
	protected WebElement buttonMyAccount;

	@FindBy(linkText = "Sign Up")
	protected WebElement buttonSignUp;
		
	
	
	public HomePage clickLogoSite()
	{
		iLogoSite.click();
		reportLog("Clicked on the site logo",true);
		return new HomePage();
	}
	
	protected void clickMyAccountButton()
	{		
//		buttonMyAccount.click();
		ExcelElement.get("Account Button").click();
		wait.until(ExpectedConditions.attributeToBe(buttonMyAccount, "class","open"));
		reportLog("Clicked on My Account button",true);
	}
	
	public RegisterPageObject clickSignUp()
	{
		clickMyAccountButton();
		buttonSignUp.click();
		reportLog("Clicked on Register",true);
		
		return new RegisterPageObject();
	}
	
	protected void reportLog(String sMessage,boolean pass){
		
		if(pass){
			try {
				String ScreenshotName = CaptureScreenshot.capture(TestBaseClass.getDriver(), generateRandomImage());
				reporter.log(Status.PASS, sMessage, MediaEntityBuilder.createScreenCaptureFromPath(ScreenshotName).build());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else
		{
			try {
				String ScreenshotName = CaptureScreenshot.capture(TestBaseClass.getDriver(), generateRandomImage());
				reporter.log(Status.FAIL, sMessage, MediaEntityBuilder.createScreenCaptureFromPath(ScreenshotName).build());
				throw new Error(sMessage,new Exception(sMessage));
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


		
	}
	public static String generateRandomImage(){
		Random r = new Random();
		double num = r.nextInt(999999999)+1000;
		return Double.toString(num);
	}
	protected void scrollToElement(WebElement element){
		JavascriptExecutor executor = (JavascriptExecutor)driver;
		executor.executeScript("arguments[0].scrollIntoView(false);", element);
	}
	
	protected String makeUpperChar(String text){
		StringBuilder sb = new StringBuilder(text);
		for (int index = 0; index < sb.length(); index++) {
		    char c = sb.charAt(index);
		    if (Character.isLowerCase(c)) {
		        sb.setCharAt(index, Character.toUpperCase(c));
		    }
		}
		return sb.toString();
	}

	
}
