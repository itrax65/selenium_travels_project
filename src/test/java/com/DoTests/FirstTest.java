package com.DoTests;



import org.testng.annotations.Test;

import com.itay.Utils.DataProviderClass;
import com.itay.Utils.TestBaseClass;
import com.itay.Utils.generatorStrings;

import pagesRepository.PageNavigator;


public class FirstTest extends TestBaseClass
{
	@Test(dataProvider = "UserInfo",dataProviderClass=DataProviderClass.class)
	public void DoTest(String firstname,String lastname,String phonenumber,String email,String password)
	{
		PageNavigator
		.HomePage()
		.clickSignUp()
		.submitForForms(firstname, lastname, phonenumber, generatorStrings.generateMail(), password, password)
		.clickOnSubmitButton()
		.verifyTextInAccountWindow(firstname, lastname)
		.verifyTextAccountButton(firstname)
		.verifyLogoExists()
		.VerifyAccountPage().VerifyAccountPage(firstname, lastname, phonenumber, email, password);
		
	}
}
