package com.itay.Utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ExcelElementProvider {
	
	public HashMap<String,ExcelElementHolder> getElements(){
		try {
			HashMap<String,ExcelElementHolder> elements = new HashMap<String,ExcelElementHolder>();
			@SuppressWarnings("resource")
			XSSFWorkbook work = new XSSFWorkbook(new FileInputStream("src/main/resources/Elements.xlsx"));
			XSSFSheet sheet = work.getSheet("Elements");
			
			
			int nRowCount = 1;	
			while(sheet.getLastRowNum() + 1 != nRowCount){
				elements.put(sheet.getRow(nRowCount).getCell(0).getStringCellValue()
						,new ExcelElementHolder(SelectorConvertor.convertSelector(nRowCount),
								sheet.getRow(nRowCount).getCell(1).getStringCellValue(),
								nRowCount));
				nRowCount++;
			}
			return elements;
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;
	}

}
