package com.itay.Utils;


import org.testng.annotations.DataProvider;

import testParameters.Autontication.XmlParametersAutontication;

public class DataProviderClass {
	
	@DataProvider(name="UserInfo")
	public static Object[][] getDataFromXml()
	{
		XmlParametersAutontication oUserInfo = new XmlParametersAutontication();
		
		return (Object[][])oUserInfo.getUserInfo();
		
	}

}
