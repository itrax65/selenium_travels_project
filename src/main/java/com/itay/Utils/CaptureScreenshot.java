package com.itay.Utils;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class CaptureScreenshot {

	public static String capture(WebDriver driver,String screenshotName) throws IOException
	{
		TakesScreenshot ts = (TakesScreenshot)driver;
		File source = ts.getScreenshotAs(OutputType.FILE);
		String dest = "C:/Users/itayPC/Desktop/Big Projects/Selenium-PHP travels/test-output/ExtentReports/ScreenShots/"+screenshotName+".png";
		File destination = new File(dest);
		FileUtils.copyFile(source, destination);
		return dest;
	}
}
//C:/Users/itayPC/Desktop/Big Projects/Selenium-PHP travels//com.itay-test-frame/test-output/ExtentReports/ScreenShots‬