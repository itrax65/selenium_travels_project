package com.itay.Utils;

import java.util.Random;

public class generatorStrings {
	public static String generateMail(){
		
		char[] chars = "abcdefghijklmnopqrstuvwxyz".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 10; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		sb.append('@');
		
		for (int i = 0; i < 5; i++) {
		    char c = chars[random.nextInt(chars.length)];
		    sb.append(c);
		}
		
		sb.append(".com");
		
		String output = sb.toString();
		
		return output;
	}
}
