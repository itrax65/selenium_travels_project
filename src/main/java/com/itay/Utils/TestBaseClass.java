package com.itay.Utils;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.*;

public class TestBaseClass extends ReportClassConfig 
{
	public static WebDriver driver;	
	
	@BeforeMethod(alwaysRun = true)
	public void InstallTest()
	{
		  System.setProperty("webdriver.chrome.driver", "C:/Users/itayPC/Desktop/Big Projects/chromedriver.exe");
//		  ChromeOptions options = new ChromeOptions();
//		  options.addArguments("--start-maximized");
		  driver = new ChromeDriver();
		  driver.manage().window().maximize();
		  
		  XmlReaderConfigManager rXmlReader = new XmlReaderConfigManager();
		  
		  driver.manage().timeouts().pageLoadTimeout(ConstVaribles.SECONDS_LOAD_PAGE, TimeUnit.SECONDS);
		  
		  driver.manage().timeouts().implicitlyWait(ConstVaribles.SECONDS_EXPLICIT_WAIT, TimeUnit.SECONDS);
		  
		  driver.get(rXmlReader.getUrl().toString());

	}
	
	@AfterMethod(alwaysRun = true)
	public void AfterTest()
	{
		driver.quit();
	}
	public static WebDriver getDriver()
	{
		return driver;
	}
}
