package com.itay.Utils;

public class ExcelElementHolder {
	
	private Selectors selector;
	private String sLocator;
	private int iRow;
	
	public ExcelElementHolder(Selectors selector,String sLocator,int iRow){
		this.selector = selector;
		this.sLocator = sLocator;
		this.iRow = iRow;
	}
	public Selectors getSelector(){return selector;}
	public String getLocator(){return sLocator;}
	public int getRow(){return iRow;}
}
