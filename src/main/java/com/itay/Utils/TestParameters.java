package com.itay.Utils;

 public class TestParameters {
	
	 public static String sFirstName,sLastName,sPhoneNumber,sEmail,sPassword;
	
	/**
	 * Set parameters for user based data driven testing.
	 * The test is being done several times, until all parameters
	 * are tested from the Idenfitication.xml
	 * @param firstname String that represents the firstname
	 * @param lastname String that represents the lastname
	 * @param mobilenumber String that represents the phone number
	 * @param email String that represents the email
	 * @param password String that represents the password
	 */
	 public static void RegisterParameters(String firstname,String lastname,String phonenumber,String email,
			String password){
		sFirstName = firstname;
		sLastName = lastname;
		sPhoneNumber = phonenumber;
		sEmail = email;
		sPassword = password;
	}

}
