package com.itay.Utils;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class SelectorConvertor {
	
	@SuppressWarnings("resource")
	public static Selectors convertSelector(int row){
		
		XSSFWorkbook work;
		try {
			work = new XSSFWorkbook(new FileInputStream("src/main/resources/Elements.xlsx"));
			XSSFSheet sheet = work.getSheet("Elements");
			
			String selector = sheet.getRow(row).getCell(2).getStringCellValue();
			
			if(selector.equals("CSS"))
				return Selectors.CSS;
			
			if(selector.equals("NAME"))
				return Selectors.NAME;
			
			
			
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return null;

	}

}
