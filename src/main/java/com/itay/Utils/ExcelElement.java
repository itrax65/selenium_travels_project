package com.itay.Utils;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

public class ExcelElement {
	
	static ExcelElementProvider excel;
	public static WebElement get(String name){
		excel = new ExcelElementProvider();
		
		switch(excel.getElements().get(name).getSelector()){
		case CSS:
			return TestBaseClass.getDriver().findElement(By.cssSelector(
					excel.getElements().get(name).getLocator()));

		case ID:
			return TestBaseClass.getDriver().findElement(By.id(
					excel.getElements().get(name).getLocator()));
		case NAME:
			return TestBaseClass.getDriver().findElement(By.name(
					excel.getElements().get(name).getLocator()));

		case XPATH:
			return TestBaseClass.getDriver().findElement(By.xpath(
					excel.getElements().get(name).getLocator()));

		default:
			return null;
			
		}
	}

}
