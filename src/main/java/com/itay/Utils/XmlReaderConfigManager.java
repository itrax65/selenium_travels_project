package com.itay.Utils;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class XmlReaderConfigManager {
	
	String sUrl;
	
	public XmlReaderConfigManager(){
	
		try {
			
			File fXmlFile = new File("src/main/resources/config.xml");
			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = (Document) dBuilder.parse(fXmlFile);
			
			doc.getDocumentElement().normalize();
			
			NodeList nList = doc.getElementsByTagName("properties");
			
			for(int i = 0;i<nList.getLength();i++)
			{
				Node nNode = nList.item(i);
				
				if(nNode.getNodeType()==Node.ELEMENT_NODE)
				{
					Element eElement = (Element) nNode;
					
					sUrl = eElement.getElementsByTagName("url").item(0).getTextContent();
					
				}
				
			}
			
			
		} catch (ParserConfigurationException e) {
			
			e.printStackTrace();
		} catch (SAXException ex) {

			ex.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		
	}
	
	public String getUrl(){
		return sUrl;
	}
	
}
