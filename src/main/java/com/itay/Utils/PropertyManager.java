package com.itay.Utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class PropertyManager {
	
	String sUrl;
	Properties prop = new Properties();
	InputStream input = null;
	
	public PropertyManager()
	{
		try
		{
			input = new FileInputStream("/com.itay-test-frame/src/main/resources/com/itay/Utils/Properties.properties");
			{
				System.out.println("Did not find File");
			}
			input.close();

			prop.load(input);
			
			sUrl = prop.getProperty("url");

			
			input.close();
			
		}
		catch(IOException ex)
		{
			System.out.println("file wasnt loaded properly");
		}
		finally
		{

		}
	}
	public String getUrl()
	{
		return sUrl;
	}

	
}
