package testParameters.Autontication;

import java.io.File;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public class XmlParametersAutontication {
	
	Object[][] oUserInfo;
	
	public XmlParametersAutontication()
	{
		
		
		try
		{
		File fXmlFile = new File("src/main/resources/idenfitication.xml");
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
		Document doc = (Document) dBuilder.parse(fXmlFile);
		
		
		doc.getDocumentElement().normalize();
		
		NodeList nList = doc.getElementsByTagName("PersonalInfo");
		
		oUserInfo = new Object[nList.getLength()][5];
		
		for(int i =0;i<nList.getLength();i++)
		{
			Node nNode = nList.item(i);
			
			if(nNode.getNodeType() == Node.ELEMENT_NODE)
			{
				Element eElement = (Element)nNode;
								
				oUserInfo[i][0] = eElement.getElementsByTagName("firstname").item(0).getTextContent();
				oUserInfo[i][1] = eElement.getElementsByTagName("lastname").item(0).getTextContent();
				oUserInfo[i][2] = eElement.getElementsByTagName("mobilenumber").item(0).getTextContent();
				oUserInfo[i][3] = eElement.getElementsByTagName("email").item(0).getTextContent();
				oUserInfo[i][4] = eElement.getElementsByTagName("password").item(0).getTextContent();
				
			}
		}
		
		}
		catch(Exception e)
		{
			e.getStackTrace();
		}

	}
	
	public Object[][] getUserInfo()
	{
		return oUserInfo;
	}
	
}
